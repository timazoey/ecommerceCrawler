# -*- coding: UTF-8 -*-
import requests
import time
import json
import pandas as pd
import csv
import re
import bs4.builder

from datetime import datetime
from bs4 import BeautifulSoup
from pandas import DataFrame as df

def ehobbyScraper(keyword):
    headers = {
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
        }
    r = requests.get('https://www.ehobby.com.tw/search/'+keyword,
                 headers=headers, allow_redirects=True)

    soup = BeautifulSoup(r.text, 'html.parser')
    df = pd.DataFrame(columns=['title', 'merchant', 'price', 'soldNum', 'stock', 'imageUrl'])

    title = soup.find_all("span", class_="item_photo js_product_image")
    title_final = [f.get('title') for f in title]

    picture = soup.find_all("span", class_="item_photo js_product_image")
    picture_final = [m.get('style').replace('background-image: url(', '').replace(')','') for m in picture]

    #money = soup.find_all("div", class_="list-unstyled item price")
    #money_final = [kkk.get('span') for kkk in money]
    money = soup.find_all("ul", class_="list-unstyled item_price")
    
    for moneys in range(len(title_final)):
        moneys_to_html = bs4.BeautifulSoup(money.__str__(), 'lxml')

        sub_money1 = moneys_to_html.find_all("span", class_="font_montserrat")
        sub_money1_final = [a.text for a in sub_money1]

        sub_money2 = moneys_to_html.find_all("li", class_="item_soldout")
        sub_money2_final = [b.text for b in sub_money2]

        money_combine = sub_money1_final + sub_money2_final
        
    
        df.loc[moneys] = [title_final[moneys], '',money_combine[moneys], '', '', picture_final[moneys]]
        print(df)
    time = str(datetime.now())
    df.to_csv(r'export/ehobby/' + time + 'ehobby.csv',index=None, header=True, encoding='utf-8')
  

    # for nnn in range(len(money)):
    #     df.loc[nnn] = [title_final[nnn], '',money_final[nnn], '', '', picture_final[nnn]]
    #     print(df)

    # time = str(datetime.now())
    # df.to_csv(r'export/ehobby/' + time + 'ehobby.csv',
    #           index=None, header=True, encoding='utf-8')

ehobbyScraper(keyword='魯夫')
#To do :需抓到店家（）、標題(o)、價格(o)、售出數（）、庫存、圖片url(o)
