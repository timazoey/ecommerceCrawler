import os
import re
import json
import time
import random
import urllib
import requests
import pandas as pd
from retrying import retry

from simulated_login import TaoBaoLogin

# 關閉警告
requests.packages.urllib3.disable_warnings()
# 登入與爬取需要使用同一個Session對象
req_session = requests.Session()
# 淘寶商品excel文件保存路徑
GOODS_EXCEL_PATH = 'taobao_goods.xlsx'


class GoodsSpider:
    def __init__(self, q):
        self.q = q
        # 超過時間
        self.timeout = 15
        self.good_list = []
        # 淘寶登入
        tbl = TaoBaoLogin(req_session)
        tbl.login()

    @retry(stop_max_attempt_number=3)
    def spider_goods(self, page):
        """

        :param page: 淘寶分頁的參數
        :return:
        """
        s = page * 44

        # 搜尋連結，q參數表示搜尋的關鍵字， s = page * 44 數據索引開始
        search_url = f'https://s.taobao.com/search?initiative_id=tbindexz_20170306&ie=utf8&spm=a21bo.2017.201856-taobao-item.2&sourceId=tb.index&search_type=item&ssid=s5-e&commend=all&imgfile=&q={self.q}&suggest=history_1&_input_charset=utf-8&wq=biyunt&suggest_query=biyunt&source=suggest&bcoffset=4&p4ppushleft=%2C48&s={s}&data-key=s&data-value={s + 44}'

        # 代理ip 網路上搜尋一個，  站大爷：http://ip.zdaye.com/dayProxy.html
        # 盡量使用最新的代理池，某些ip可能沒辦法使用。後期可以考慮做一個ip池
        # 爬取淘寶ip要求需要很高，代理免費ip基本上都不能使用，如果不能爬取請更換ip

        proxies = {'http': '23.80.121.202:30201',
                   'https': '124.156.111.19:27954'
                   }

        # 請求 headers
        headers = {
            'referer': 'https://www.taobao.com/',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'

        }
        response = req_session.get(search_url, headers=headers, proxies=proxies,
                                   verify=False, timeout=self.timeout)
        # print(response.text)
        goods_match = re.search(r'g_page_config = (.*?)}};', response.text)
        # 判斷如果無匹配數據
        if not goods_match:
            print('提取頁面中的數據失敗！')
            print(response.text)
            raise RuntimeError
        goods_str = goods_match.group(1) + '}}'
        goods_list = self._get_goods_info(goods_str)
        self._save_excel(goods_list)
        # print(goods_str)

    def _get_goods_info(self, goods_str):
        """
        解析json數據，並提取標題、價格、商家地址、銷量、評價地址
        :param goods_str: string格式數據
        :return:
        """
        goods_json = json.loads(goods_str)
        goods_items = goods_json['mods']['itemlist']['data']['auctions']
        goods_list = []
        for goods_item in goods_items:
            goods = {'標題': goods_item['raw_title'],
                     '價格': goods_item['view_price'],
                     '地點': goods_item['item_loc'],
                     '售出數': goods_item['view_sales'],
                     '評論_url': goods_item['comment_url'],
                     '圖片_url': goods_item['pic_url']}
            goods_list.append(goods)
        return goods_list

    def _save_excel(self, goods_list):
        """
        將json數據生成excel文件
        :param goods_list: 商品數據
        :param startrow: 數據寫入開始行
        :return:
        """
        # pandas沒有對excel沒有追加模式，只能先讀後寫
        if os.path.exists(GOODS_EXCEL_PATH):
            df = pd.read_excel(GOODS_EXCEL_PATH)
            df = df.append(goods_list)
        else:
            df = pd.DataFrame(goods_list)

        writer = pd.ExcelWriter(GOODS_EXCEL_PATH)
        # columns參數用於指定生成的excel中列的顺序
        df.to_excel(excel_writer=writer, columns=['標題', '價格', '地點', '售出數', '評論_url', '圖片_url'], index=False,
                    encoding='utf-8', sheet_name='Sheet')
        writer.save()
        writer.close()

    def patch_spider_goods(self):
        """
        批量爬取淘寶商品
        如果爬取20多頁不能爬，可以分段爬取
        :return:
        """
        # 寫入數據前先清空之前數據
        if os.path.exists(GOODS_EXCEL_PATH):
            os.remove(GOODS_EXCEL_PATH)
        # 批量爬取，自己嘗試時建議先爬取3頁先試試
        for i in range(0, 100):
            print('第%d頁' % (i + 1))
            self.spider_goods(i)
            # 設置一個時間間隔
            time.sleep(random.randint(20, 30))


if __name__ == '__main__':
    gs = GoodsSpider('耳飾')
    gs.patch_spider_goods()
