import numpy as np
import pandas as pd
import jieba.analyse
from pyecharts import options as opts
from pyecharts.globals import SymbolType
from pyecharts.charts import Pie, Bar, Map, WordCloud

# 淘寶商品原先excel文件保存路徑
GOODS_EXCEL_PATH = 'taobao_goods.xlsx'
# 淘寶商品標準excel文件保存路徑
GOODS_STANDARD_EXCEL_PATH = 'taobao_goods_standard.xlsx'
# 清洗詞
STOP_WORDS_FILE_PATH = 'stop_words.txt'
# 讀取標準數據
DF_STANDARD = pd.read_excel(GOODS_STANDARD_EXCEL_PATH)


def standard_data():
    """
    處理淘寶爬取下來的原生數據
    例如：
        1.5万人付款 -> 15000
        广州 广州 -> 广州
    :return:
    """
    df = pd.read_excel(GOODS_EXCEL_PATH)
    # 1. 將價格轉為整數型
    raw_sales = df['售出數'].values
    new_sales = []
    for sales in raw_sales:
        sales = sales[:-3]
        sales = sales.replace('+', '')
        if '万' in sales:
            sales = sales[:-1]
            if '.' in sales:
                sales = sales.replace('.', '') + '000'
            else:
                sales = sales + '0000'
        sales = int(sales)
        new_sales.append(sales)
    df['售出數'] = new_sales
    print(df['售出數'].values)

    # 2. 將地區轉化成只包含省
    raw_location = df['地點'].values
    new_location = []
    for location in raw_location:
        if ' ' in location:
            location = location[:location.find(' ')]
        new_location.append(location)
    # df.location和df[location]效果類似
    # df.location = new_location 改成下面語句即可
    df['地點'] = new_location
    print(df['地點'].values)

    # 3. 生成新的excel
    writer = pd.ExcelWriter(GOODS_STANDARD_EXCEL_PATH)
    # columns參數用來指定生成的excel中列表的順序
    df.to_excel(excel_writer=writer, columns=['標題', '價格', '地點', '售出數', '評論_url', '圖片_url'], index=False,
                encoding='utf-8', sheet_name='Sheet')
    writer.save()
    writer.close()


def analysis_title():
    """
    詞雲分析商品標題
    :return:
    """
    # 引入全局數據
    global DF_STANDARD
    # 數據清洗，無效詞去掉
    jieba.analyse.set_stop_words(STOP_WORDS_FILE_PATH)
    # 1.詞數統計
    keywords_count_list = jieba.analyse.textrank(''.join(DF_STANDARD.標題), topK=30, withWeight=True)
    print(keywords_count_list)
    # 生成詞雲
    word_cloud = (
        WordCloud()
            .add("", keywords_count_list, word_size_range=[20, 100], shape=SymbolType.DIAMOND)
            .set_global_opts(title_opts=opts.TitleOpts(title="耳飾功能詞雲TOP30"))
    )
    word_cloud.render('詞雲圖.html')

    # 耳飾商品標題高詞頻率分析成柱狀圖
    # 2.1 統計詞數
    # 取前30高頻的關鍵詞
    keywords_count_dict = {i[0]: 0 for i in reversed(keywords_count_list[:30])}
    cut_words = jieba.cut(' '.join(DF_STANDARD.標題))
    for word in cut_words:
        for keyword in keywords_count_dict.keys():
            if word == keyword:
                keywords_count_dict[keyword] = keywords_count_dict[keyword] + 1
    print(keywords_count_dict)
    # 2.2 生成柱狀圖
    keywords_count_bar = (
        Bar()
            .add_xaxis(list(keywords_count_dict.keys()))
            .add_yaxis("", list(keywords_count_dict.values()))
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right"))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="耳飾功能TOP30"),
            yaxis_opts=opts.AxisOpts(name="功能／形容"),
            xaxis_opts=opts.AxisOpts(name="商品数")
        )
    )
    keywords_count_bar.render('高頻關鍵字和商品數量關係.html')

    # 3.標題高頻關鍵字和平均銷量關係
    keywords_sales_dict = analysis_title_keywords(keywords_count_list, '售出數', 30)
    # 生成柱狀圖
    keywords_sales_bar = (
        Bar()
            .add_xaxis(list(keywords_sales_dict.keys()))
            .add_yaxis("", list(keywords_sales_dict.values()))
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right"))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="耳飾商品功能和平均銷量TOP30"),
            yaxis_opts=opts.AxisOpts(name="功能"),
            xaxis_opts=opts.AxisOpts(name="平均销量")
        )
    )
    keywords_sales_bar.render('標題高頻關鍵字和平均銷量關係.html')


def analysis_title_keywords(keywords_count_list, column, top_num) -> dict:
    """
    分析標題關鍵字和其他屬性的關係
    :param keywords_count_list:關鍵字列表
    :param column:需要分析的屬性名
    :param top_num:擷取前多少個
    :return:
    """

    # 1.獲取高頻率詞，生成一個字典 dict={'keyword1':[], 'keyword2':[], ...}
    keywords_column_dict = {i[0]: [] for i in keywords_count_list}
    for row in DF_STANDARD.iterrows():
        for keyword in keywords_column_dict.keys():
            if keyword in row[1].標題:
                # 2.將標題包含關鍵字的屬性值放在列表中，dict={'keyword1':[屬性值1, 屬性值2, ...]}
                keywords_column_dict[keyword].append(row[1][column])
    # 3. 求屬性的平均值， dict={'keyword1':平均值1, 'keyword2':平均值2, ...}
    for keyword in keywords_column_dict.keys():
        keyword_column_list = keywords_column_dict[keyword]
        keywords_column_dict[keyword] = sum(keyword_column_list) / len(keyword_column_list)
    # 4. 根據平均值排序，從小到大
    keywords_price_dict = dict(sorted(keywords_column_dict.items(), key=lambda d: d[1]))

    # 5. 擷取平均值最高的20個關鍵字
    keywords_price_dict = {k: keywords_price_dict[k] for k in list(keywords_price_dict.keys())[-top_num:]}
    print(keywords_price_dict)
    return keywords_price_dict


def analysis_price():
    """
    分析商品價格
    :return:
    """
    # 引入全局數據
    global DF_STANDARD
    # 設置價錢切分區域
    price_list_bins = [0, 20, 40, 60, 80, 100, 120, 150, 200, 1000000]
    # 設置價錢切分過後對應到的標籤
    price_list_labels = ['0-20', '21-40', '41-60', '61-80', '81-100', '101-120', '121-150', '151-200', '200以上']
    # 分區統計
    price_count = cut_and_sort_data(price_list_bins, price_list_labels, DF_STANDARD.價格)
    print(price_count)
    # 生成柱狀圖
    bar = (
        Bar()
            .add_xaxis(list(price_count.keys()))
            .add_yaxis("", list(price_count.values()))
            .set_global_opts(
            title_opts=opts.TitleOpts("商品價格區間分佈柱狀體"),
            yaxis_opts=opts.AxisOpts(name="商品數量：個"),
            xaxis_opts=opts.AxisOpts(name="商品售價：元/人民幣"),
        )
    )
    bar.render('商品價格區間柱狀圖.html')
    # 生成餅圖
    age_count_list = [list(z) for z in zip(price_count.keys(),
                                           price_count.values()
                                           )]
    pie = (
        Pie()
            .add("", age_count_list)
            .set_global_opts(title_opts=opts
                             .TitleOpts(title="商品價格區間餅圖"))
            .set_series_opts(label_opts=opts.LabelOpts(formatter="價格區間：{b}/人民幣, 數量：{c}個"))
    )
    pie.render('商品價格區間餅圖.html')


def analysis_sale():
    """
    銷量情況分佈
    :return:
    """
    # 引入全局數據
    global DF_STANDARD
    # 設置切分區域
    listBins = [0, 200, 500, 1000, 5000, 10000, 50000]
    # 设置切分後對應之標籤
    listLabels = ['兩百以內', '兩百到五百', '五百到一千', '一千到五千', '五千到一萬', '一萬到五萬']
    # 分區統計
    sales_count = cut_and_sort_data(listBins, listLabels, DF_STANDARD.售出數)
    print(sales_count)
    # 生成柱狀圖
    bar = (
        Bar()
            .add_xaxis(list(sales_count.keys()))
            .add_yaxis("", list(sales_count.values()))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="商品銷量區間分佈柱狀圖"),
            yaxis_opts=opts.AxisOpts(name="商品個數"),
            xaxis_opts=opts.AxisOpts(name="銷售件數")
        )
    )
    bar.render('商品銷量區間分佈柱狀圖.html')
    # 生成餅圖
    age_count_list = [list(z) for z in zip(sales_count.keys(), sales_count.values())]
    pie = (
        Pie()
            .add("", age_count_list)
            .set_global_opts(title_opts=opts.TitleOpts(title="商品銷量區間餅圖"))
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}"))
    )
    pie.render('商品銷量區間餅圖.html')


def analysis_price_sales():
    """
    商品價格和銷量關係分析
    :return:
    """
    # 引入全局數據
    global DF_STANDARD
    df = DF_STANDARD.copy()
    df['group'] = pd.qcut(df.價格, 12)
    df.group.value_counts().reset_index()
    df_group_sales = df[['售出數', 'group']].groupby('group').mean().reset_index()
    df_group_str = [str(i) for i in df_group_sales['group']]
    print(df_group_str)
    # 生成柱狀圖
    bar = (
        Bar()
            .add_xaxis(df_group_str)
            .add_yaxis("", list(df_group_sales['售出數']), category_gap="50%")
            .set_global_opts(title_opts=opts
                             .TitleOpts(title="商品價格分區和平均銷量柱狀圖"),
                             yaxis_opts=opts.AxisOpts(name="平均銷量"),
                             xaxis_opts=opts.AxisOpts(name="價格區間"),
                             axispointer_opts={"rotate": 30}
                             )
    )
    bar.render('商品價格分區和平均銷量柱狀圖.html')


def cut_and_sort_data(listBins, listLabels, data_list) -> dict:
    """
    統計list中的元素個數，返回元素和count
    :param listBins: 數據切分區域
    :param listLabels: 切分後對應標籤
    :param data_list: 數據列表形式
    :return: key為元素 value為count的dict
    """
    data_labels_list = pd.cut(data_list, bins=listBins, labels=listLabels, include_lowest=True)
    # 生成一個以listLabels為順序的字典，這樣就不需要後面重新排序
    data_count = {i: 0 for i in listLabels}
    # 統計結果
    for value in data_labels_list:
        # get(value, num)函數的作用是獲取字典中value對應的值, num=0指示初始值大小。
        data_count[value] = data_count.get(value) + 1
    return data_count


def analysis_province_sales():
    """
    省份和銷量的分佈
    :return:
    """
    # 引入全局數據
    global DF_STANDARD

    # 1. 全國商家數量分佈
    province_sales = DF_STANDARD.地點.value_counts()
    province_sales_list = [list(item) for item in province_sales.items()]
    print(province_sales_list)

    # 1.1 生成熱力圖
    province_sales_map = (
        Map()
            .add("商家數量全國分佈圖", province_sales_list, "china")
            .set_global_opts(
            visualmap_opts=opts.VisualMapOpts(max_=1600)

        )
    )
    province_sales_map.render("商家數量全國分佈圖.html")


if __name__ == '__main__':
    # 数据清洗
    standard_data()
    # 數據分析
    analysis_title()
    analysis_price()
    analysis_sale()
    analysis_price_sales()
    analysis_province_sales()
