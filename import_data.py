# -*- coding: UTF-8 -*-
import sqlite3
import csv
import os
import os.path

conn = sqlite3.connect('data.db')
c = conn.cursor()

#send champtoys data to db
with open("./export/champtoys/{}".format('champtoys.csv'), newline='') as csvfile:
  rows = csv.DictReader(csvfile, skipinitialspace=True)
  headers = rows.fieldnames

  for entry in rows:
        try:
            c.execute('INSERT INTO champtoys VALUES (?,?,?,?,?,?)', (
        entry['title'], entry['merchant'], entry['price'], entry['soldNum'], entry['stock'],
        entry['imageUrl']
      ))
            conn.commit()
        except sqlite3.IntegrityError:
            pass

#send shopee data to db
with open("./export/shopee/{}".format('shopee.csv'), newline='') as csvfile:
  rows = csv.DictReader(csvfile, skipinitialspace=True)
  headers = rows.fieldnames

  for entry in rows:
        try:
            c.execute('INSERT INTO shopee VALUES (?,?,?,?,?,?)', (
                entry['title'], entry['merchant'], entry['price'], entry['soldNum'], entry['stock'],
                entry['imageUrl']
            ))
            conn.commit()
        except sqlite3.IntegrityError:
            pass

#send myacg data to db
with open("./export/myacg/{}".format('myacg.csv'), newline='') as csvfile:
  rows = csv.DictReader(csvfile, skipinitialspace=True)
  headers = rows.fieldnames

  for entry in rows:
        try:
            c.execute('INSERT INTO myacg VALUES (?,?,?,?,?,?)', (
                entry['title'], entry['merchant'], entry['price'], entry['soldNum'], entry['stock'],
                entry['imageUrl']
            ))
            conn.commit()
        except sqlite3.IntegrityError:
            pass

#send ruten data to db
with open("./export/ruten/{}".format('ruten.csv'), newline='') as csvfile:
  rows = csv.DictReader(csvfile, skipinitialspace=True)
  headers = rows.fieldnames

  for entry in rows:
        try:
            c.execute('INSERT INTO ruten VALUES (?,?,?,?,?,?)', (
                entry['title'], entry['merchant'], entry['price'], entry['soldNum'], entry['stock'],
                entry['imageUrl']
            ))
            conn.commit()
        except sqlite3.IntegrityError:
            pass

#send ehobby data to db
with open("./export/ehobby/{}".format('ehobby.csv'), newline='') as csvfile:
  rows = csv.DictReader(csvfile, skipinitialspace=True)
  headers = rows.fieldnames

  for entry in rows:
        try:
            c.execute('INSERT INTO ehobby VALUES (?,?,?,?,?,?)', (
                entry['title'], entry['merchant'], entry['price'], entry['soldNum'], entry['stock'],
                entry['imageUrl']
            ))
            conn.commit()
        except sqlite3.IntegrityError:
            pass

# #send taobao data to db
# with open("./export/taobao/{}".format('taobao.csv'), newline='') as csvfile:
#   rows = csv.DictReader(csvfile, skipinitialspace=True)
#   headers = rows.fieldnames

#   for entry in rows:
#         try:
#             c.execute('INSERT INTO taobao VALUES (?,?,?,?,?,?)', (
#                 entry['title'], entry['merchant'], entry['price'], entry['soldNum'], entry['stock'],
#                 entry['imageUrl']
#             ))
#             conn.commit()
#         except sqlite3.IntegrityError:
#             pass

