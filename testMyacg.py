# -*- coding: UTF-8 -*-
import requests
import time
import json
import pandas as pd
import csv
import re
import bs4.builder

from datetime import datetime
from bs4 import BeautifulSoup
from pandas import DataFrame as df

def myacgScraper(keyword):
        headers = {
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
        }
        r = requests.get('https://www.myacg.com.tw/goods_list.php?keyword='+keyword,
                 headers=headers, allow_redirects=True)


        soup = BeautifulSoup(r.text, 'html.parser')
        df = pd.DataFrame(columns=['title', 'merchant', 'price', 'soldNum', 'stock', 'imageUrl'])

        title = soup.find_all("div", class_="name")
        title_final = [i.find('a').get_text() for i in title]
        #print(title_final)

        price = soup.find_all("p", class_="price")
        price_final = [j.find('span').get_text() for j in price]
        #print(price_final)
        picture = soup.find_all("div", class_="pic")
        picture_to_html = bs4.BeautifulSoup(picture.__str__(), 'lxml')
        picture1 = picture_to_html.find('a')
        pictures = [k.get('src') for k in picture1]

        gid = soup.find_all("div", class_="pic")
        gid_final = [f.find('a').get('href') for f in gid]
        
        for nnn in range(len(gid_final)): 
            r1 = requests.get('https://www.myacg.com.tw/'.join(gid_final), headers=headers, allow_redirects=True)
            soup1 = BeautifulSoup(r1.text, 'html.parser')
            print(soup1)
            
            
            soldNum = soup1.find_all("span", class_="t_bold")
            soldNum_final = [g.get_text().replace('\t', '').replace('\r\n', '') for g in soldNum]
            print(soldNum)
           

            
            #url = 'https://www.myacg.com.tw/' + pictures[nnn]
            #print(pictures)
            
            #print(len(title_final))
            #print(len(price_final))
            #print(len(url))

            #df.loc[nnn] = [title_final[nnn],  price_final[nnn], '', soldNum_final[nnn],  '',  url]
            #print(df)

        #time = str(datetime.now()) 
        #df.to_csv(r'export/myacg/' + time +'myacg.csv', index=None, header=True, encoding='utf-8')   


myacgScraper(keyword='魯夫')
#To do :需抓到店家（）、標題(o)、價格(o)、售出數（）、庫存、圖片url
