# -*- coding: UTF-8 -*-
import time
import requests
import json
import pandas as pd
import glob
from datetime import datetime


from selenium import webdriver
from bs4 import BeautifulSoup
from pandas import DataFrame as df


def shopeeAPI_Scraper(keyword, n_items):

    url1 = f'https://shopee.tw/api/v2/search_items/?by=relevancy&keyword={keyword}&limit={n_items}'
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36 Googlebot', }
    r = requests.get(url1, headers=headers)
    api1_data = json.loads(r.text)
    df = pd.DataFrame(columns=['title', 'merchant', 'price', 'soldNum', 'stock', 'imageUrl'])

    for i in range(n_items):
        itemid = api1_data['items'][i]['itemid']
        shopid = api1_data['items'][i]['shopid']

        url2 = f'https://shopee.tw/api/v2/item/get?itemid={itemid}&shopid={shopid}'
        r = requests.get(url2, headers=headers)
        api2_data = json.loads(r.text)

        url3 = f'https://shopee.tw/api/v2/item/get_ratings?filter=0&flag=1&itemid={itemid}&limit=6&offset=0&shopid={shopid}&type=0'
        r3 = requests.get(url3, headers=headers)
        api3_data = json.loads(r3.text)

        url4 = f'https://shopee.tw/api/v2/shop/get?is_brief=1&shopid={shopid}'
        r4 = requests.get(url4, headers=headers)
        api4_data = json.loads(r4.text)

        product_name = api2_data['item']['name']
        imageid = api2_data['item']['images'][0]
        merchant = api4_data['data']['name']
        stock = api2_data['item']['stock']
        title = api1_data['items'][i]['name'].ljust(50)
        money = api2_data['item']['price']/100000
        #location = api2_data['item']['shop_location']
        sold_num = api2_data['item']['historical_sold']
        comment = 'https://shopee.tw/' + product_name + '-i.' + str(shopid) + '.' + str(itemid)
        url = 'https://cf.shopee.tw/file/' + str(imageid) + '_tn'
        
        df.loc[i] = [title, merchant, money, sold_num, stock, url]
        print(df)
        
    time = str(datetime.now()) 
    #df.to_excel(r'export/shopee/' + time +'shopee.xlsx', index=None, header=True, encoding='utf-8')
    df.to_csv(r'export/shopee/' + time + 'shopee.csv',index=None, header=True, encoding='utf-8')


shopeeAPI_Scraper(keyword='皮卡丘', n_items=100)
#To do :需抓到店家（）、標題(o)、價格(o)、售出數（）、庫存、圖片url
